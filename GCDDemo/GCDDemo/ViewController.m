//
//  ViewController.m
//  GCDDemo
//
//  Created by 吳瀾洲 on 2020/12/24.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self serialQueue2];
    
    /**
     
     
     DISPATCH_QUEUE_SERIAL     //串行队列
     DISPATCH_QUEUE_CONCURRENT    //并行队列
     
     
     //串行队列
     dispatch_queue_t serialQueue = dispatch_queue_create("com.lysongzi.serial", DISPATCH_QUEUE_SERIAL);

     //并行队列
     dispatch_queue_t concurrentQueue = dispatch_queue_create("com.lysongzi.concurrent", DISPATCH_QUEUE_CONCURRENT);
     
     
     DISPATCH_QUEUE_PRIORITY_HIGH 2，优先级高
     DISPATCH_QUEUE_PRIORITY_DEFAULT 0，默认优先级，中
     DISPATCH_QUEUE_PRIORITY_LOW -2，优先级低
     DISPATCH_QUEUE_PRIORITY_BACKGROUND INT16_MIN，后台模式，优先级最低
     
     
     */
    
}

//并发队列(并发会开启多个线程,那个线程执行那个任务由队列决定),执行的顺序我们无法控制
- (void)concurrentQueue1{
    dispatch_queue_t queue = dispatch_queue_create("com.lai.www", DISPATCH_QUEUE_CONCURRENT);
         
        for (int i = 0; i< 10;i++){
            // 10个异步
            dispatch_async(queue, ^{
                NSLog(@"%@--%d",[NSThread currentThread], i);
            });
        }
}
//并发队列，使用同步方式执行任务则和串行队列一样
- (void)concurrentQueue2{
    dispatch_queue_t queue = dispatch_queue_create("com.lai.www", DISPATCH_QUEUE_CONCURRENT);
        
        for (int i = 0; i< 10;i++){
            // 10个同步
            dispatch_sync(queue, ^{
                NSLog(@"%@--%d",[NSThread currentThread], i);
            });
        }
}

//串行队列(只有一个线程执行任务),任务按顺序执行
- (void)serialQueue1{
    
    dispatch_queue_t queue = dispatch_queue_create("com.lai.www", DISPATCH_QUEUE_SERIAL);
     
        for (int i = 0; i< 10;i++){
            // 10个异步
            dispatch_async(queue, ^{
                NSLog(@"%@--%d",[NSThread currentThread], i);
            });
        }
}
//串行队列
- (void)serialQueue2{
    dispatch_queue_t serialQueue = dispatch_queue_create("com.lai.www", DISPATCH_QUEUE_SERIAL);
        
        dispatch_async(serialQueue, ^{
            sleep(3);
            NSLog(@"%@--1",[NSThread currentThread]);
        });
        dispatch_sync(serialQueue, ^{
            
            sleep(1);
            NSLog(@"%@--2",[NSThread currentThread]);
            
        });
        dispatch_async(serialQueue, ^{
            NSLog(@"%@--3",[NSThread currentThread]);
        });
        dispatch_sync(serialQueue, ^{
            sleep(5);
            NSLog(@"%@--4",[NSThread currentThread]);
        });
        
        dispatch_async(serialQueue, ^{
            
            NSLog(@"%@--5",[NSThread currentThread]);
        });
}
//串行队列(同步不会创建新的线程，而异步会创建新的线程，且只创建一个)
- (void)serialQueue3{
    //创建串行队列,传入参数为DISPATCH_QUEUE_SERIAL
    dispatch_queue_t   serialQueue = dispatch_queue_create("com.lysongzi.serial", DISPATCH_QUEUE_SERIAL);

            //同步执行队列中的任务，会立即在当前线程执行该任务
            dispatch_sync(serialQueue, ^{
                NSLog(@"这里是同步执行的串行队列01。===> %@", [NSThread currentThread]);
            });

            //异步执行任务，会新开一个线程，多个任务按顺序执行
            dispatch_async(serialQueue, ^{
                for (int i = 0; i < 3; i++) {
                    NSLog(@"串行01 ===> %d ===> %@", i, [NSThread currentThread]);
                }
            });

            dispatch_sync(serialQueue, ^{
                NSLog(@"这里是同步执行的串行队列02。===> %@", [NSThread currentThread]);
            });

            dispatch_async(serialQueue, ^{
                for (int i = 0; i < 3; i++) {
                    NSLog(@"串行02 ===> %d ===> %@", i, [NSThread currentThread]);
                }
            });

            dispatch_async(serialQueue, ^{
                for (int i = 0; i < 3; i++) {
                    NSLog(@"串行03 ===> %d ===> %@", i, [NSThread currentThread]);
                }
            });
}

@end
