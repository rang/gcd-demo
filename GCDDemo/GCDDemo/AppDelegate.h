//
//  AppDelegate.h
//  GCDDemo
//
//  Created by 吳瀾洲 on 2020/12/24.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

